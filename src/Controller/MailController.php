<?php

namespace App\Controller;

use App\Entity\Bear;
use App\Form\BearType;
use App\Repository\BearRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/mail")
 */
class MailController extends AbstractController
{
    /**
     * @Route("/", name="mail", methods={"GET"})
     */
    public function mail(\Swift_Mailer $mailer): Response
    {

        $message = (new \Swift_Message('Hello Email'))
            ->setFrom('send@example.com')
            ->setTo('recipient@example.com')
            ->setBody('Bonjour','text/html')
        ;

        $mailer->send($message);

        return new Response('ok');
    }

}
