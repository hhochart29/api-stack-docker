import React from 'react';
import ReactDOM from 'react-dom';
import Differencify from 'differencify';
import App from './App';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

const differencify = new Differencify({
  debug: true,
});

(async () => {
  try {
    const result = await differencify
    .init()
    .launch({ headless: true })
    .newPage()
    .setViewport({
      width: 1600,
      height: 1200,
    })
    .goto('http://localhost:3000')
    .waitFor(1000)
    .screenshot()
    .toMatchSnapshot()
    .result((result2) => {
      console.log(result2);
    })
    .close()
    .end();
  } catch (e) {
    console.error(e)
  }

})();
